#create database summativedb and use it
CREATE DATABASE summativedb;
use summativedb;
#create table student
CREATE TABLE student(
	id INT NOT NULL auto_increment PRIMARY KEY,
    name VARCHAR(25),
    surname VARCHAR(20),
    birthdate DATE,
    gender VARCHAR(10)
);
#create table lesson
CREATE TABLE lesson(
	id INT NOT NULL auto_increment PRIMARY KEY,
    name VARCHAR(25),
    level INT
);
#create table score
CREATE TABLE score(
	id INT NOT NULL auto_increment PRIMARY KEY,
    student_id INT,
    lesson_id INT,
    score INT,
    FOREIGN KEY (student_id) references student(id),
    FOREIGN KEY (lesson_id) references lesson(id)
);
#insert data to table student
INSERT INTO student
	(name,surname,birthdate,gender)
    VALUES
    ("Jonathan","Gabriel","1996-01-07","pria"),
	("Wendi","Samosir","1996-02-01","pria"),
	("Ivan","Rusli","1995-07-04","pria"),
	("Zhang","Fei","1932-08-21","wanita"),
	("Xin","Chai","1934-09-17","wanita");
SELECT * FROM student;
#insert data to table lesson
INSERT INTO lesson 
	(name,level)
	VALUES
	("Matematika",10),("Fisika",11),("Biologi",9),
	("Bahasa Indonesia",8),("Bahasa Inggris",7);
SELECT * FROM lesson;
#insert data to table score
INSERT INTO score
	(student_id,lesson_id,score)
    VALUES
    (1,1,80),(1,2,75),(2,3,65),(2,4,80),(3,5,75),
    (3,1,70),(4,2,86),(4,3,75),(5,1,87),(5,2,75);
SELECT * FROM score;