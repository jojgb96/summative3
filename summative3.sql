#create table employee
CREATE TABLE employee(
	EMPLOYEE_ID INT PRIMARY KEY auto_increment NOT NULL,
    FIRST_NAME VARCHAR(20),
    LAST_NAME VARCHAR(20),
    EMAIL VARCHAR(25),
    PHONE_NUMBER VARCHAR(15),
    HIRE_DATE DATE,
    JOB_ID VARCHAR(11),
    SALARY DECIMAL(8,2),
    COMMISION_PCT DECIMAL(3,2),
    MANAGER_ID INT,
    DEPARTEMENT_ID INT
);

SELECT * FROM employee;
/*
Harus diakses pake mysql --Local-infile -u root -p di cmd(ya saya sih pakenya gitbash biar bisa copas dari sini)
*/

#buat baca file dari luar
LOAD DATA LOCAL INFILE 'C:\\Users\\Nexsoft\\Desktop\\summative2.txt'
INTO TABLE employee
FIELDS TERMINATED BY '|'
LINES TERMINATED BY '|' starting by '|';