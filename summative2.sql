SELECT
	s.name,s.surname,s.gender,
	l.level as class,l.name as lesson_name,
    sc.score
FROM student s
JOIN score sc /*pake alias*/
ON s.id = sc.student_id
JOIN lesson l
ON sc.lesson_id = l.id; 
